<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'frontendlab');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.ZZwv,(eN+bVxwo)=5en+z5Ev!*#mSn4S|k@T@S026lnGq%jrt|?XFx]Yh$WNah?');
define('SECURE_AUTH_KEY',  '@x}jZ0#EJPS^e&l(GbYLBX%fKGE=(ax:7&2kRWE4l!wsb,-c`H$]E=eMSV1Mz2em');
define('LOGGED_IN_KEY',    '-6t&jkzh<=C75:[G1eu@iz-AJ4[<9$ujt<I)h}V#sY>bRe&Zn.1=BjbEzCw(fo;|');
define('NONCE_KEY',        '7.x@p.d4?qmQme^Q38v#X0ag)^>%QJqSK,Z#q}UoI+ [!H+&UZ+K9Uw<tajk=)XZ');
define('AUTH_SALT',        '^4$-#<s&2P]7ZB/rV##FFfe*,9bP-rC~1bS*yaBOn&]B1H]aYwO?l<i^%[v~}J]:');
define('SECURE_AUTH_SALT', '<hQtkt86TeQw]G[2a`t2/N%~[ID2Z3>6%~F.$cRsE?fgUe#)GSVdAQ(V}L;)k@58');
define('LOGGED_IN_SALT',   '3fhVfm.I^&XI:M?d2c0E&T,33EFXlW /5L~NfRLU@ LtX%c57,qCo6u8A>LS,$qD');
define('NONCE_SALT',       ']=6WpWnJk]/&ixEy.aV#!ls+TJu,A=C}Vx2[,DM;+jku.SAlz]lqd(i59D?k8LtX');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');